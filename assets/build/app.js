function lineTop(line, offset) {
	var line_height = $(offset).position().top - 10;
	$(line).css({top : 5}).attr({'data-height' : line_height});
}

function lineBottom(line, offset, parent) {
	var line_top   = $(offset).position().top + $(offset).outerHeight() + 10;
	var line_height = $(parent).outerHeight() - line_top;

	$(line).css({top : line_top}).attr({'data-height' : line_height});
}

function randomBetween(min, max) {
    return (Math.random() * (max - min + 1) | 0) + min;
}

function lineAnimation(element, callback) {
	if (!$(element).hasClass('animation-line')) {
		$(element).addClass('animation-line');

		$(element).animate({height: $(element).attr('data-height')}, 1300, function () {

			if (callback) {
				callback();
			}
		});
	}
}

function playVideo($section) {
	var vid = document.getElementById($section.find('video').attr('id'));

	if (vid) {
		vid.play();
        //vid.setAttribute("data-video", "play");

		vid.addEventListener('ended', function(e) {
		     pauseVideo($section);
		}, false);

		vid.addEventListener('pause', function(e) {
		     pauseVideo($section);
		}, false);

        $section.find('.video-overflow').show();

		if ($(window).outerWidth() > 1024) {
			//PC & Tablet
			$section.find('.background-device').removeAttr('style');
            TweenMax.to($section.find('.button-play'), 0.3, {autoAlpha : 0, ease : Sine.easeOut, delay : 0.2});

			TweenMax.to($section.find('.background-device, .textbox'), 0.8, {opacity : 0, delay : 0.2, ease : Sine.easeInOut, onComplete : function () {
				TweenMax.to($section.find('.video-control'), 0.3, {autoAlpha : 1, ease : Sine.easeOut, delay : 0.2});
			}});


		} else {
			//Mobile
			TweenMax.to($section.find('.background-device'), 0.3, {opacity : 0, ease : Sine.easeOut});
			$section.find('.video-overflow').css({height : $section.find('.background-mobile').outerHeight()});
			$section.find('.video-overflow').show();
		}

		if (device.mobile()) {$("html, body").animate({ scrollTop: $section.offset().top - 50 }, 500);}
	}
}

function pauseVideo($section) {
	var vid = document.getElementById($section.find('video').attr('id'));

	if (vid) {
        //$section.find('.video-overflow').hide();
        //vid.setAttribute("data-video", "pause");
        //if ($section.attr('id') != 'intro') {
           setTimeout(function () {
               $section.find('.video-control .video-control__button--play').removeClass('video-control__button--play').addClass('video-control__button--pause');
           }, 500);
        //}

		vid.pause();
	}

	if ($(window).outerWidth() > 1024) {
		//PC
		TweenMax.to($section.find('.video-control'), 0.3, {autoAlpha : 0, ease : Sine.easeOut, delay : 0.2});
		TweenMax.to($section.find('.background-device, .textbox'), 0.3, {opacity : 1, ease : Sine.easeOut});
        TweenMax.to($section.find('.button-play'), 0.3, {autoAlpha : 1, ease : Sine.easeOut});
	} else {
		// Tablet & Mobile
		TweenMax.to($section.find('.background-device'), 0.3, {opacity : 1, ease : Sine.easeOut});
		$section.find('.video-overflow').hide();
		$section.find('.video-overflow').removeAttr('style');
	}
}

function getCurrentSession() {
	for (i = 0; i < sectionScroll.length; i++) {
		if ($(window).scrollTop() <= sectionScroll[i]) {
			switch (i) {
				case 0:
					return '.intro';
					break;
				default:
					return '.section' + i;
					break;
			}
		}
	}
}

function animationTextbox(texbox, callback) {
    var perDelay = 10;
    var y = ($(window).outerWidth() > 767) ? -200 : -30;

	$($(texbox).get().reverse()).each(function(e){
		var $item = $(this);
		TweenMax.fromTo($item, 0.5, {opacity : 0}, {opacity : 1, ease: Sine.easeOut, delay: 0.3 + e/perDelay});
		TweenMax.fromTo($item, 0.7, {y : y}, {y : 0, ease: Back.easeOut, delay: 0.3 + e/perDelay});
	}).promise().done( function(){ if (callback) callback() } );
}

function slidePinnedActive(element, slide){
	$(element + ' .slide-icons__object.active').removeClass('active');
    $(element + ' .align-middle .slide-icons__object').eq(slide).addClass('active');
}

function range(start, end) {
    var arrayRange = [];
    for (var i = start; i <= end; i++) {
        arrayRange.push(i);
    }
    return arrayRange;
}

function shuffle(array) {
    var counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        var index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        var temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function time_span(time, text) {
    var obj = {};
    obj.hours = 0;
    obj.minutes = 0;
    obj.seconds = 0;

    while(time >= 3600)
    {
        obj.hours++;
        time -= 3600;
    }

    while(time >= 60)
    {
        obj.minutes++;
        time -= 60;
    }

    obj.seconds = time;

    if (obj.hours.toString().length <= 1) obj.hours = '0'+obj.hours;
    if (obj.minutes.toString().length <= 1) obj.minutes = '0'+ obj.minutes;
    if (obj.seconds.toString().length <= 1) obj.seconds = '0'+ obj.seconds;

    if (!text) {
        return obj;
    } else {
        var hours = (obj.hours != 0) ? obj.hours  : '00';
        var minutes = (obj.minutes != 0) ? obj.minutes : '00';
        var seconds = (obj.seconds != 0) ? obj.seconds  : '00';

        return hours + ':' + minutes + ':' + seconds;
    }
}
var controller;
var section = [$('.intro').outerHeight(), $('.section1').outerHeight(), $('.section2').outerHeight(), $('.section3').outerHeight(), $('.section4').outerHeight(), $('.section5').outerHeight()];
var sectionScroll = [];
var App = {
	init : function () {
        //Reload
        if (document.location.hostname == "localhost") {
            var js = document.createElement("script");

            js.type = "text/javascript";
            js.src = 'http://localhost:12345/livereload.js';

            document.body.appendChild(js);
        }

        //Init
		$.each(App, function (key, value) {
			var func = 'App.' + key + '.init';

			try {
				if ( eval("typeof "+ func +" === 'function'") ) {
					if (key != 'init') {
						eval(func)();
					}
				}
			} catch(err) {
				//return false;
			}
		});
	}
};
App.datapicker = {
    init: function() {
        if( $('.js-datetimepicker-inline').length > 0 ) {
            $('.js-datetimepicker-inline').datepicker();
        }
    }
}

App.enquire = {
    init: function() {
        $(".js-enquire-popup-open").fancybox({
    		maxWidth	: 420,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'enquire-popup-wrap'
    	});
    }
}

App.mapTours = {
    init : function () {
      if( $('#map').length > 0 ) {

          var mapGG = $('#map');
          var offset = mapGG.offset().top;

          $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            if( scrollTop > offset) {
                mapGG.addClass('fixed');
            } else {
                mapGG.removeClass('fixed');
            }
          })

      }
    }
}

App.menuMobile = {
    init : function () {
            App.menuMobile.menu();
    },
    menu : function () {

        $('.header__hbg-button').click(function () {
            $(this).toggleClass('open');
            $('.header__secondary').slideToggle();
        })

        $(document).click(function(e) {

            var viewportWidth = window.innerWidth;
            if (viewportWidth < 640) {
                if ( $(e.target).closest('.header').length === 0 ) {
                    $('.header__hbg-button').removeClass('open');
                    $('.header__secondary').slideUp();
                }
            }
        });

        $(document).on('click', '.main-menu > ul > li > a', function (e) {

            var viewportWidth = window.innerWidth;
            if (viewportWidth < 640) {

                if( $(this).siblings('ul').length > 0) {

                    if( $(this).parent().hasClass("open") ) {


                    } else {
                        $(this).parent().addClass('open').siblings().removeClass('open');
                        $(this).parent().siblings().find('ul').slideUp();

                        $(this).siblings('ul').slideDown();
                        e.preventDefault();

                    }
                }

            }

        })

    }
};


App.parallax = {
    init : function () {

        if($('.js-parallax').length > 0) {
            $('.js-parallax').stellar();
        }

    }
}

App.promotionDetail = {
    init : function() {
        $(".js-promotion-popup-open").fancybox({
    		maxWidth	: 420,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'promotion-popup-wrap'
    	});

        App.promotionDetail.moveSecondaryInfo();
        App.promotionDetail.windowResize();

    },

    moveSecondaryInfo : function() {

        var windowWidth = window.innerWidth;
        var info = $('.promotion-detail__info');

        if ( windowWidth < 640 ) {
            info.appendTo(".page-heading__cell");
        } else {
            info.appendTo(".promotion-detail .row .col-md-6:nth-child(2)");
        }

    },

    windowResize: function() {
        $(window).resize(function() {
            App.promotionDetail.moveSecondaryInfo();
        })
    }
}

App.scrollToEle = {
    init : function () {
      // if(window.location.hash) {
      //     var hash = window.location.hash.substring(1);
      //     App.scrollToEle.scrollTo('#'+hash);
      //  }
    },
    scrollTo : function(ele) {
      var element = $(ele);
      $('html, body').animate({ scrollTop: element.offset().top - 50}, 1000);
    }
}

App.select2 = {
    init : function () {

        if ( $('.js-select2').length > 0 ) {
            $('.js-select2').select2({
                minimumResultsForSearch: Infinity
            });
        }

    }

}


App.tabs = {
    init : function () {

        if($('.js-tabs').length > 0) {
            $(document).on('click', '.js-tabs a', function (e) {

                $(this).addClass("active").siblings().removeClass("active");

                var content = $(this).attr("href");
                $(content).addClass("active").siblings().removeClass("active");

                e.preventDefault();

            })
        }

    }
}

App.tourDetail = {

    init : function() {
        App.tourDetail.gallery();
        App.tourDetail.estimateFee();
        App.tourDetail.moveSecondaryInfo();
        App.tourDetail.windowResize();
        App.tourDetail.bookingPopup();
    },

    gallery : function () {

        if( $('.tour-gallery__slideshow').length > 0 ) {
            $('.tour-gallery__slideshow').on("init", function () {
                $('.tour-gallery').hide();
            })
            $('.tour-gallery__slideshow').slick({
                arrows:false,
                dots: false,
                fade:true
            });
        }

        $(document).on('click','.tour-gallery__nav a', function(e) {
            var index = $(this).parent().index();
            $('.tour-gallery__slideshow').slick("slickGoTo",index);
            e.preventDefault();
        })

        // $(document).on("click", ".js-gallery-open", function() {
        //
        // })

        $(".js-gallery-open").fancybox({
    		maxWidth	: 800,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'tour-gallery-wrap'
    	});

        $(document).on('click','.js-gallery-open', function() {
            var index = $(this).index();
            $('.tour-gallery__slideshow').slick("slickGoTo",index);
        })

    },

    estimateFee : function() {
        $(".js-estimateFee-open").fancybox({
    		maxWidth	: 400,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'estimateFee-popup'
    	});
    },

    moveSecondaryInfo : function() {

        var windowWidth = window.innerWidth;
        var info = $('.tour-detail__secondary');

        if ( windowWidth < 640 ) {
            info.appendTo(".page-heading__cell");
        } else {
            info.appendTo(".tour-detail .col-sm-5");
        }

    },

    windowResize: function() {
        $(window).resize(function() {
            App.tourDetail.moveSecondaryInfo();
        })
    },

    bookingPopup: function() {
        $(".js-booking-tour-open").fancybox({
            maxWidth	: 400,
            maxHeight	: 'auto',
            fitToView	: false,
            width		: '90%',
            height		: '90%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'fade',
            closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'booking-tour-popup-wrap'
        });
    }

}
