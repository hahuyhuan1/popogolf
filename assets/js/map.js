function initMap(_location) {
    var centerPosition = {
        lat: locations[0][1],
        lng: locations[0][2]
    };
    var zoomPosition = 7;
    var iconDefault = "assets/img/tours/marker.png";
    var iconHightlight = "assets/img/tours/marker-hl.png";

    // console.log(_location);

    if (typeof _location != "undefined") {
        centerPosition = {
            lat: _location[1],
            lng: _location[2]
        };
        zoomPosition = 7;
    }
    centerPosition.lng = centerPosition.lng - 2;

    console.log(centerPosition);
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoomPosition,
        center: centerPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow({});

    var marker, i;

    for (i = 0; i < locations.length; i++) {

        if (i == locations.indexOf(_location)) {

            marker = new google.maps.Marker({
                position: {
                    lat: locations[i][1],
                    lng: locations[i][2]
                },
                map: map,
                icon: iconHightlight
            });

        } else {

            marker = new google.maps.Marker({
                position: {
                    lat: locations[i][1],
                    lng: locations[i][2]
                },
                map: map,
                icon: iconDefault
            });

        }

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}

function toLocation(index) {
    initMap(locations[index]);
}

$(document).on("mouseenter", ".tour-item-horizontal__wrap", function() {
    var index = $(this).attr("id").split("-")[1];
    toLocation(index);
})

$(document).on("click", ".tour-item-horizontal__wrap", function() {
    // alert(1);
    $(this).parent().addClass("active").siblings().removeClass("active");
})
