App.enquire = {
    init: function() {
        $(".js-enquire-popup-open").fancybox({
    		maxWidth	: 420,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'enquire-popup-wrap'
    	});
    }
}
