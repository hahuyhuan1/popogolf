App.menuMobile = {
    init : function () {
            App.menuMobile.menu();
    },
    menu : function () {

        $('.header__hbg-button').click(function () {
            $(this).toggleClass('open');
            $('.header__secondary').slideToggle();
        })

        $(document).click(function(e) {

            var viewportWidth = window.innerWidth;
            if (viewportWidth < 640) {
                if ( $(e.target).closest('.header').length === 0 ) {
                    $('.header__hbg-button').removeClass('open');
                    $('.header__secondary').slideUp();
                }
            }
        });

        $(document).on('click', '.main-menu > ul > li > a', function (e) {

            var viewportWidth = window.innerWidth;
            if (viewportWidth < 640) {

                if( $(this).siblings('ul').length > 0) {

                    if( $(this).parent().hasClass("open") ) {


                    } else {
                        $(this).parent().addClass('open').siblings().removeClass('open');
                        $(this).parent().siblings().find('ul').slideUp();

                        $(this).siblings('ul').slideDown();
                        e.preventDefault();

                    }
                }

            }

        })

    }
};
