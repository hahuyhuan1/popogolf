
App.tabs = {
    init : function () {

        if($('.js-tabs').length > 0) {
            $(document).on('click', '.js-tabs a', function (e) {

                $(this).addClass("active").siblings().removeClass("active");

                var content = $(this).attr("href");
                $(content).addClass("active").siblings().removeClass("active");

                e.preventDefault();

            })
        }

    }
}
