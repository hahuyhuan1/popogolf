App.promotionDetail = {
    init : function() {
        $(".js-promotion-popup-open").fancybox({
    		maxWidth	: 420,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'promotion-popup-wrap'
    	});

        App.promotionDetail.moveSecondaryInfo();
        App.promotionDetail.windowResize();

    },

    moveSecondaryInfo : function() {

        var windowWidth = window.innerWidth;
        var info = $('.promotion-detail__info');

        if ( windowWidth < 640 ) {
            info.appendTo(".page-heading__cell");
        } else {
            info.appendTo(".promotion-detail .row .col-md-6:nth-child(2)");
        }

    },

    windowResize: function() {
        $(window).resize(function() {
            App.promotionDetail.moveSecondaryInfo();
        })
    }
}
