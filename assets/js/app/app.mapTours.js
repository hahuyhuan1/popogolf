App.mapTours = {
    init : function () {
      if( $('#map').length > 0 ) {

          var mapGG = $('#map');
          var offset = mapGG.offset().top;

          $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            if( scrollTop > offset) {
                mapGG.addClass('fixed');
            } else {
                mapGG.removeClass('fixed');
            }
          })

      }
    }
}
