function lineTop(line, offset) {
	var line_height = $(offset).position().top - 10;
	$(line).css({top : 5}).attr({'data-height' : line_height});
}

function lineBottom(line, offset, parent) {
	var line_top   = $(offset).position().top + $(offset).outerHeight() + 10;
	var line_height = $(parent).outerHeight() - line_top;

	$(line).css({top : line_top}).attr({'data-height' : line_height});
}

function randomBetween(min, max) {
    return (Math.random() * (max - min + 1) | 0) + min;
}

function lineAnimation(element, callback) {
	if (!$(element).hasClass('animation-line')) {
		$(element).addClass('animation-line');

		$(element).animate({height: $(element).attr('data-height')}, 1300, function () {

			if (callback) {
				callback();
			}
		});
	}
}

function playVideo($section) {
	var vid = document.getElementById($section.find('video').attr('id'));

	if (vid) {
		vid.play();
        //vid.setAttribute("data-video", "play");

		vid.addEventListener('ended', function(e) {
		     pauseVideo($section);
		}, false);

		vid.addEventListener('pause', function(e) {
		     pauseVideo($section);
		}, false);

        $section.find('.video-overflow').show();

		if ($(window).outerWidth() > 1024) {
			//PC & Tablet
			$section.find('.background-device').removeAttr('style');
            TweenMax.to($section.find('.button-play'), 0.3, {autoAlpha : 0, ease : Sine.easeOut, delay : 0.2});

			TweenMax.to($section.find('.background-device, .textbox'), 0.8, {opacity : 0, delay : 0.2, ease : Sine.easeInOut, onComplete : function () {
				TweenMax.to($section.find('.video-control'), 0.3, {autoAlpha : 1, ease : Sine.easeOut, delay : 0.2});
			}});


		} else {
			//Mobile
			TweenMax.to($section.find('.background-device'), 0.3, {opacity : 0, ease : Sine.easeOut});
			$section.find('.video-overflow').css({height : $section.find('.background-mobile').outerHeight()});
			$section.find('.video-overflow').show();
		}

		if (device.mobile()) {$("html, body").animate({ scrollTop: $section.offset().top - 50 }, 500);}
	}
}

function pauseVideo($section) {
	var vid = document.getElementById($section.find('video').attr('id'));

	if (vid) {
        //$section.find('.video-overflow').hide();
        //vid.setAttribute("data-video", "pause");
        //if ($section.attr('id') != 'intro') {
           setTimeout(function () {
               $section.find('.video-control .video-control__button--play').removeClass('video-control__button--play').addClass('video-control__button--pause');
           }, 500);
        //}

		vid.pause();
	}

	if ($(window).outerWidth() > 1024) {
		//PC
		TweenMax.to($section.find('.video-control'), 0.3, {autoAlpha : 0, ease : Sine.easeOut, delay : 0.2});
		TweenMax.to($section.find('.background-device, .textbox'), 0.3, {opacity : 1, ease : Sine.easeOut});
        TweenMax.to($section.find('.button-play'), 0.3, {autoAlpha : 1, ease : Sine.easeOut});
	} else {
		// Tablet & Mobile
		TweenMax.to($section.find('.background-device'), 0.3, {opacity : 1, ease : Sine.easeOut});
		$section.find('.video-overflow').hide();
		$section.find('.video-overflow').removeAttr('style');
	}
}

function getCurrentSession() {
	for (i = 0; i < sectionScroll.length; i++) {
		if ($(window).scrollTop() <= sectionScroll[i]) {
			switch (i) {
				case 0:
					return '.intro';
					break;
				default:
					return '.section' + i;
					break;
			}
		}
	}
}

function animationTextbox(texbox, callback) {
    var perDelay = 10;
    var y = ($(window).outerWidth() > 767) ? -200 : -30;

	$($(texbox).get().reverse()).each(function(e){
		var $item = $(this);
		TweenMax.fromTo($item, 0.5, {opacity : 0}, {opacity : 1, ease: Sine.easeOut, delay: 0.3 + e/perDelay});
		TweenMax.fromTo($item, 0.7, {y : y}, {y : 0, ease: Back.easeOut, delay: 0.3 + e/perDelay});
	}).promise().done( function(){ if (callback) callback() } );
}

function slidePinnedActive(element, slide){
	$(element + ' .slide-icons__object.active').removeClass('active');
    $(element + ' .align-middle .slide-icons__object').eq(slide).addClass('active');
}

function range(start, end) {
    var arrayRange = [];
    for (var i = start; i <= end; i++) {
        arrayRange.push(i);
    }
    return arrayRange;
}

function shuffle(array) {
    var counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        var index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        var temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function time_span(time, text) {
    var obj = {};
    obj.hours = 0;
    obj.minutes = 0;
    obj.seconds = 0;

    while(time >= 3600)
    {
        obj.hours++;
        time -= 3600;
    }

    while(time >= 60)
    {
        obj.minutes++;
        time -= 60;
    }

    obj.seconds = time;

    if (obj.hours.toString().length <= 1) obj.hours = '0'+obj.hours;
    if (obj.minutes.toString().length <= 1) obj.minutes = '0'+ obj.minutes;
    if (obj.seconds.toString().length <= 1) obj.seconds = '0'+ obj.seconds;

    if (!text) {
        return obj;
    } else {
        var hours = (obj.hours != 0) ? obj.hours  : '00';
        var minutes = (obj.minutes != 0) ? obj.minutes : '00';
        var seconds = (obj.seconds != 0) ? obj.seconds  : '00';

        return hours + ':' + minutes + ':' + seconds;
    }
}