App.tourDetail = {

    init : function() {
        App.tourDetail.gallery();
        App.tourDetail.estimateFee();
        App.tourDetail.moveSecondaryInfo();
        App.tourDetail.windowResize();
        App.tourDetail.bookingPopup();
    },

    gallery : function () {

        if( $('.tour-gallery__slideshow').length > 0 ) {
            $('.tour-gallery__slideshow').on("init", function () {
                $('.tour-gallery').hide();
            })
            $('.tour-gallery__slideshow').slick({
                arrows:false,
                dots: false,
                fade:true
            });
        }

        $(document).on('click','.tour-gallery__nav a', function(e) {
            var index = $(this).parent().index();
            $('.tour-gallery__slideshow').slick("slickGoTo",index);
            e.preventDefault();
        })

        // $(document).on("click", ".js-gallery-open", function() {
        //
        // })

        $(".js-gallery-open").fancybox({
    		maxWidth	: 800,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'tour-gallery-wrap'
    	});

        $(document).on('click','.js-gallery-open', function() {
            var index = $(this).index();
            $('.tour-gallery__slideshow').slick("slickGoTo",index);
        })

    },

    estimateFee : function() {
        $(".js-estimateFee-open").fancybox({
    		maxWidth	: 400,
    		maxHeight	: 'auto',
    		fitToView	: false,
    		width		: '90%',
    		height		: '90%',
    		autoSize	: false,
    		closeClick	: false,
    		openEffect	: 'fade',
    		closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'estimateFee-popup'
    	});
    },

    moveSecondaryInfo : function() {

        var windowWidth = window.innerWidth;
        var info = $('.tour-detail__secondary');

        if ( windowWidth < 640 ) {
            info.appendTo(".page-heading__cell");
        } else {
            info.appendTo(".tour-detail .col-sm-5");
        }

    },

    windowResize: function() {
        $(window).resize(function() {
            App.tourDetail.moveSecondaryInfo();
        })
    },

    bookingPopup: function() {
        $(".js-booking-tour-open").fancybox({
            maxWidth	: 400,
            maxHeight	: 'auto',
            fitToView	: false,
            width		: '90%',
            height		: '90%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'fade',
            closeEffect	: 'fade',
            padding     : 0,
            wrapCSS : 'booking-tour-popup-wrap'
        });
    }

}
