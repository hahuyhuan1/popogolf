App.select2 = {
    init : function () {

        if ( $('.js-select2').length > 0 ) {
            $('.js-select2').select2({
                minimumResultsForSearch: Infinity
            });
        }

    }

}
