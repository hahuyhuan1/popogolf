'use strict';

var DIR = 'assets/';
var gulpRanInThisFolder = process.cwd();
var dirArray = gulpRanInThisFolder.split("\\");
var dirFolder = dirArray[dirArray.length - 1];

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    watch = require('gulp-watch');

gulp.task('app-css', function () {
  return gulp.src(DIR + 'sass/*.scss')
    // .pipe(autoprefixer({
    //     browsers: ['last 2 versions'],
    //     cascade: false
    // }))
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(DIR + 'build'))
    .pipe(browserSync.stream());
});

gulp.task('app-js', function () {
   return gulp.src([
       DIR + 'js/app/include/**/*.js',
       DIR + 'js/app/app.js',
       DIR + 'js/app/app.section.js',
       DIR + 'js/app/**/*.js'
   ])
      //.pipe(uglify())
      .pipe(concat('app.js'))
      .pipe(gulp.dest(DIR + 'build/'))
      .pipe(browserSync.stream());
});

gulp.task('lib-js', function () {
   return gulp.src([
     DIR + 'lib/app/jquery/jquery.min.js',
     DIR + 'lib/app/**/*.js',
   ])
      .pipe(uglify())
      .pipe(concat('lib.js'))
      .pipe(gulp.dest(DIR + 'build/'))
      .pipe(browserSync.stream());
});

gulp.task('lib-css', function () {
   return gulp.src([
     DIR + 'lib/app/**/*.css',
   ])
      .pipe(concat('lib.css'))
      .pipe(gulp.dest(DIR + 'build/'))
      .pipe(browserSync.stream());
});

gulp.task('watch', function () {
   gulp.watch(DIR + 'sass/**/*.scss', ['app-css']);
   gulp.watch(DIR + 'js/**/*.js', ['app-js']);
   gulp.watch(DIR + 'lib/app/**/*.js', ['lib-js','lib-css']);
   gulp.watch('**/*.html').on('change', browserSync.reload);
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: 'localhost'
    });
});

gulp.task('default', ['lib-js','lib-css','app-js', 'app-css', 'watch', 'browser-sync'], function(){
   // Default task code
});
